using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed;
    public float size;
    public Text countText;

    private Rigidbody rb; 
    private int count;

    public Button shrinkButton, resetButton, growButton;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        size = 1;
        SetCountText ();
        //initialize buttons
        shrinkButton.onClick.AddListener(Shrink);
        resetButton.onClick.AddListener(Reset);
        growButton.onClick.AddListener(Grow);
    }

    // Update is called once per frame
    void Update()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");
        if (Input.GetKeyDown("1"))
        {
            Shrink();
        }
        if (Input.GetKeyDown("2"))
        {
            Reset();
            // print(size);
        }
        if (Input.GetKeyDown("3"))
        {
            Grow();
        }
        //Update
        transform.localScale = new Vector3(size, size, size);
        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        rb.AddForce (movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString ();
    }
    void Shrink()
    {
        size -= 0.5f;
        if (size <= 0.5f)
        {
            size = 0.5f;
        }
    }
    void Reset()
    {
        size = 1;
    }
    void Grow()
    {
        size += 0.5f;
        if (size >= 3.0f)
        {
            size = 3.0f;
        }
    }
}
